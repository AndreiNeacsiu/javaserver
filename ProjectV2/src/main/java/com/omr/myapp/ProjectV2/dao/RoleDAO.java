package com.omr.myapp.ProjectV2.dao;

import java.util.List;

import com.omr.myapp.ProjectV2.model.Role;

public interface RoleDAO {
	public Role addRole(Role role);
	public Role updateRole(Role role);
	public List<Role> getRoles();
	public void removeRole(int id);
}
