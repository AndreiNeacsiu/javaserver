package com.omr.myapp.ProjectV2.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import com.omr.myapp.ProjectV2.model.Role;
@Service("roleDAO")
public class RoleDAOImpl implements RoleDAO {
	
	@PersistenceContext(unitName="demoRestPersistence")
	private EntityManager entityManager;
	
	private String sqlString = null;
	@Override
	public Role addRole(Role role) {
		return entityManager.merge(role);
	}

	@Override
	public Role updateRole(Role role) {
		return entityManager.merge(role);
	}

	@Override
	public List<Role> getRoles() {
		sqlString = "SELECT r FROM Role r";
		TypedQuery<Role> query = entityManager.createQuery(sqlString, Role.class);		

		return query.getResultList();
	}

	@Override
	public void removeRole(int id) {
		Role role = entityManager.find(Role.class, id);
		entityManager.remove(role);
		
	}

}	
