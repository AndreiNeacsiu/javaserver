package com.omr.myapp.ProjectV2.dao;

import java.util.List;

import com.omr.myapp.ProjectV2.model.Token;


public interface TokenDAO {
	public Token addToken(Token token);
	public Token updateToken(Token token);
	public List<Token> getTokens();
	public void removeToken(int id);
	public Token findByToken(String token);
}
