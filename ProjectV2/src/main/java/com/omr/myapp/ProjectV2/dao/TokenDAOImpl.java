package com.omr.myapp.ProjectV2.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Service;

import com.omr.myapp.ProjectV2.model.Token;
@Service("tokenDAO")
public class TokenDAOImpl implements TokenDAO{
	
	@PersistenceContext(unitName="demoRestPersistence")
	private EntityManager entityManager;
	
	private String sqlString = null;

	public Token addToken(Token token) {
		return entityManager.merge(token);
	}


	public Token updateToken(Token token) {
		return entityManager.merge(token);
	}

	@Override
	public List<Token> getTokens() {
		sqlString = "SELECT t FROM Token t";
		TypedQuery<Token> query = entityManager.createQuery(sqlString, Token.class);		

		return query.getResultList();
	}

	@Override
	public void removeToken(int id) {
		Token token = entityManager.find(Token.class, id);
		entityManager.remove(token);
		
	}

	@Override
	public Token findByToken(String token) {
		try {
			sqlString = "FROM Token t where t.token = ?1";
			TypedQuery<Token> query = entityManager.createQuery(sqlString, Token.class);		
			query.setParameter(1, token);
			return query.getSingleResult();
			
		}catch (Exception e) {
			return null;
		}
	}



}
