package com.omr.myapp.ProjectV2.dao;

import java.util.List;

import com.omr.myapp.ProjectV2.model.User;

public interface UserDAO {

	public User addUser(User user);
	public User updateUser(User u);
	public List<User> getUsers();
	public List<User> getPagesUsers(int id);
	public int getNumberUser();
	public User getUserById(int id);
	public void removeUser(int id);
	public User findByUsername(String username);
}

