package com.omr.myapp.ProjectV2.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.omr.myapp.ProjectV2.model.User;
@Service("userDAO")
public class UserDAOImpl implements UserDAO {
	
	@PersistenceContext(unitName="demoRestPersistence")
	private EntityManager entityManager;

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	private String sqlString = null;
	
	public User findByUsername(String username){
		try {
			sqlString = "FROM User u JOIN FETCH u.role where u.username = ?1";
			TypedQuery<User> query = entityManager.createQuery(sqlString, User.class);		
			query.setParameter(1, username);
			return query.getSingleResult();
			
		}catch (Exception e) {
			return null;
		}
		
	}
	public User addUser(User user) {
		return entityManager.merge(user);
	}
	public User updateUser(User user) {
		user.setPassword((bCryptPasswordEncoder.encode(user.getPassword())));
		return entityManager.merge(user);
	}
	public int getNumberUser(){
		sqlString = "FROM User u JOIN FETCH u.role";
		TypedQuery<User> query = entityManager.createQuery(sqlString, User.class);				

		return  query.getResultList().size();
	}
	public List<User> getUsers() {
		sqlString = "FROM User u JOIN FETCH u.role";
		TypedQuery<User> query = entityManager.createQuery(sqlString, User.class);		
		return query.getResultList();
	}
	public List<User> getPagesUsers(int id) {
		sqlString = "FROM User u JOIN FETCH u.role";
		TypedQuery<User> query = entityManager.createQuery(sqlString, User.class);		
		List<User> userList = query.getResultList();
		if(userList.size() > id*10 + 10 ){
			return userList.subList(id*10, (id*10) + 10);
		}
		else if(userList.size() > id*10 ){
			return userList.subList(id*10, userList.size());
		}	
		else{
			return null;
		}	
	}

	public User getUserById(int id) {
		try {
			sqlString = "SELECT u FROM User u WHERE u.id = ?1";
			TypedQuery<User> query = entityManager.createQuery(sqlString, User.class);		
			query.setParameter(1, id);
			return query.getSingleResult();
			
		}catch (NoResultException e) {
			return null;
		}

	}
	public void removeUser(int id) {
		User user = entityManager.find(User.class, id);
		entityManager.remove(user);
	}

}
