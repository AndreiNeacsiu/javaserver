package com.omr.myapp.ProjectV2.filtres;

import java.io.IOException;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.ext.Provider;

import org.springframework.beans.factory.annotation.Autowired;

import com.omr.myapp.ProjectV2.dao.TokenDAO;
import com.omr.myapp.ProjectV2.model.Token; 

@Secured
@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter {
	@Autowired
	private TokenDAO tokenDAO;
	
    public void filter(ContainerRequestContext requestContext) throws IOException {

        // Get the HTTP Authorization header from the request
        String authorizationHeader = 
            requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        // Check if the HTTP Authorization header is present and formatted correctly 
        if (authorizationHeader == null || !authorizationHeader.startsWith("Bearer ")) {
            throw new NotAuthorizedException("Authorization header must be provided");
        }

        //Extract the token from the HTTP Authorization header
        String token = authorizationHeader.substring("Bearer".length()).trim();
        
        Token tk = tokenDAO.findByToken(token);
        if(tk == null)
        {
        	throw new NotAuthorizedException("Authorization token must be provided");
        }
      //  System.out.println(token);
      //  Key signingKey = securityService.generateKey();	
     //   try {

        	//securityService.verifyToken(token, signingKey);

//        } catch (Exception e) {
//            requestContext.abortWith(
//                Response.status(Response.Status.UNAUTHORIZED).build());
//        }
    }
}
