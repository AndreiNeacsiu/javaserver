package com.omr.myapp.ProjectV2.resource;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.omr.myapp.ProjectV2.credential.Credentials;
import com.omr.myapp.ProjectV2.filtres.Secured;
import com.omr.myapp.ProjectV2.model.Token;
import com.omr.myapp.ProjectV2.model.User;
import com.omr.myapp.ProjectV2.service.UserService;

/**
 * Root resource (exposed at "myresource" path)
 */


@Qualifier
@Component
@Path("myresource")
public class UserResourceAPI {	
	@Autowired
	private UserService userService;
	
	@POST
    @Path("authentication")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.TEXT_HTML})
	@Transactional
    public Response test(Credentials credentials){
        try {
        	Token tk = userService.svalidateUser(credentials.getUsername(), credentials.getPassword());
            return Response.ok(tk.getToken()).build();

        } catch (Exception e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }      
      	
    }
  
    @GET
    @Secured
    @Path("usersPage/{id}")
    @Produces({ MediaType.APPLICATION_JSON})
    public Response userPage(@PathParam("id") int id){
        GenericEntity<List<User>> entity = 
                new GenericEntity<List<User>>(Lists.newArrayList(userService.sgetPagesUsers(id))) {};
                return Response.ok(entity).build();
    }
    @GET
    @Secured
    @Path("usersNumber")
    @Produces({ MediaType.TEXT_HTML})
    public Response getNumberUser(){
    	return Response.status(Response.Status.OK) // 200
				.entity(Integer.toString(userService.sgetNumberUser())).build();
    }
    @GET
    @Secured
    @Path("get")
    @Produces({ MediaType.APPLICATION_JSON})
    public Response getUser(){
    	//return userDAO.getUsers();
        GenericEntity<List<User>> entity = 
                new GenericEntity<List<User>>(Lists.newArrayList(userService.sgetUsers())) {};
                return Response.ok(entity).build();
    }
    @POST
    @Secured
    @Path("add")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON  })
    @Transactional
    public Response add(User user) {
    	return Response.status(Response.Status.CREATED) // 201
				.entity(userService.ssaveUpdateUser(user)).build();
    }
    @PUT
    @Secured
    @Path("update")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON  })
    @Transactional
    public Response update(User user) {
    	return Response.status(Response.Status.OK) // 200
				.entity(userService.ssaveUpdateUser(user)).build();
    }
    @DELETE
    @Secured
    @Path("remove/{id}")
    @Produces(MediaType.TEXT_HTML)
    @Transactional
    public Response remove(@PathParam("id") int id){
    	userService.sremoveUser(id);
    	return Response.status(Response.Status.OK) // 200
				.entity("User was successfully deleted").build();
    }
}
