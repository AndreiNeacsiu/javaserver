package com.omr.myapp.ProjectV2.service;

import java.io.IOException;
import java.util.List;

import com.omr.myapp.ProjectV2.model.Token;
import com.omr.myapp.ProjectV2.model.User;

public interface UserService {

	public User ssaveUpdateUser(User user);//done
	public void sremoveUser(int id);
	public List<User> sgetUsers();
	public List<User> sgetPagesUsers(int id);
	public int sgetNumberUser();
	public User sgetUserById(int id);
	public Token svalidateUser(String username, String password) throws IOException;
}
