package com.omr.myapp.ProjectV2.service;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import javax.ws.rs.NotAuthorizedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.omr.myapp.ProjectV2.dao.RoleDAO;
import com.omr.myapp.ProjectV2.dao.TokenDAO;
import com.omr.myapp.ProjectV2.dao.UserDAO;
import com.omr.myapp.ProjectV2.model.Token;
import com.omr.myapp.ProjectV2.model.User;
@Service("userService")
public class UserServiceImpl implements UserService{

	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private RoleDAO roleDAO;
	
	@Autowired
	private UserDAO userDAO;
	
	@Autowired
	private TokenDAO tokenDAO;
	
	public Token svalidateUser(String username, String password)throws IOException{
		User u = userDAO.findByUsername(username);

		if(!bCryptPasswordEncoder.matches(password, u.getPassword()))
			throw new NotAuthorizedException("Wrong Credentials");
		
    	Token tk = new Token();
    	String token = UUID.randomUUID().toString();
    	tk.setUser_id(u.getId());
    	tk.setToken(token);
    	tk.setDatetime(null);
    	tk.setLast_updated(null);
    	tokenDAO.addToken(tk);
		return tk;
	}
	
	public User ssaveUpdateUser(User user) {
		user.setPassword((bCryptPasswordEncoder.encode(user.getPassword())));	
		user.setRoles(new HashSet<>(roleDAO.getRoles()));
		return userDAO.addUser(user);
	}

	@Override
	public void sremoveUser(int id) {
		 userDAO.removeUser(id);
	}

	@Override
	public List<User> sgetUsers() {
		return userDAO.getUsers();
	}

	@Override
	public List<User> sgetPagesUsers(int id) {
		return userDAO.getPagesUsers(id);
	}

	@Override
	public int sgetNumberUser() {
		return userDAO.getNumberUser();
	}

	@Override
	public User sgetUserById(int id) {
		return userDAO.getUserById(id);
	}
	
}
